let customButton = document.getElementById('customButton');
let customCode = ['font: 23px Impact;', 'color: #000000;', 'background-color: #f1f5f5;', 'border-radius: 8%;', 'border: 1px solid #0080ff;', 'padding: 10px;', 'box-shadow: 2px 2px 3px #18151e;'];

let customButtonH = document.getElementById('customButtonH');
let customCodeH = [];

hoverSwitch();

//function to print each item in customCode(); called at the end of all of the small functions
function fillCode() {
  let codeArea = document.getElementById('codeArea');
  codeArea.innerHTML = "";

  //prints each item in code list
  customCode.forEach(function(item){
    //print everything in the array except ones marked "false"
    if (item !== 'false') {
      codeArea.innerHTML += ('&nbsp;&nbsp;' + item + '<br>');
    }
  });
}

//changes text of custom button
function buttonTextChange() {
  let buttonText = document.getElementById('buttonText').value;

  customButton.innerHTML = buttonText;
  customButtonH.innerHTML = buttonText;
}

function font() {
  let fontSize = document.getElementById('fontSize').value;
  let fontFamily = document.getElementById('fontFamily').value;
  let boldCheck = document.getElementById('boldCheck').checked;
  let italicCheck = document.getElementById('italicCheck').checked;
  let boldCheckH = document.getElementById('boldCheckH').checked;
  let italicCheckH = document.getElementById('italicCheckH').checked;

  let isBold = boldCheck ? 'bold' : '';
  let isItalic = italicCheck ? 'italic' : '';
  let fontEmphasis = isBold + ' ' + isItalic;

  //font styling
  customButton.style.font = fontEmphasis + ' ' + fontSize + 'px ' + fontFamily;
  customCode[0] = `font: ${fontEmphasis} ${fontSize}px ${fontFamily};`;
  customButtonH.style.font = fontSize + 'px ' + fontFamily;

  //bold & italic settings for :hover
  if (boldCheckH) {
    customButtonH.style.fontWeight = 'bold';
    customCodeH[0] = 'font-weight: bold;';
  } else {
    customButtonH.style.fontWeight = 'normal';
    customCodeH[0] = 'false';
  }

  if (italicCheckH) {
    customButtonH.style.fontStyle = 'italic';
    customCodeH[1] = 'font-style: italic;';
  } else {
    customButtonH.style.fontStyle = 'normal';
    customCodeH[1] = 'false';
  }

  fillCode();
  fillCodeH();
}

//text color
function fontColor() {
  let fontColor = document.getElementById('fontColor').value;
  let fontColorH = document.getElementById('fontColorH').value;

  customButton.style.color = fontColor;
  customCode[1] = `color: ${fontColor};`;

  customButtonH.style.color = fontColorH;

  fillCode();
  fillCodeH();
}

//box color
function boxColors() {
  let gradientCheck = document.getElementById('gradientCheck').checked;
  let bgColor1 = document.getElementById('bgColor1').value;
  let bgColor2 = document.getElementById('bgColor2');
  let bgColor2Label = document.getElementById('bgColor2Label');

  let gradientCheckH = document.getElementById('gradientCheckH').checked;
  let bgColor1H = document.getElementById('bgColor1H').value;
  let bgColor2H = document.getElementById('bgColor2H');
  let bgColor2LabelH = document.getElementById('bgColor2LabelH');

  if (gradientCheck){
    //enable gradient colors
    bgColor2.disabled = false;
    bgColor2Label.classList.remove('disabled');

    customButton.style.background = getCssValuePrefix() + 'linear-gradient( ' +
      bgColor1 + ', ' + bgColor2.value + ')';

    customCode[2] =
      `background: -webkit-linear-gradient(${bgColor1}, ${bgColor2.value});<br>` +
      `&nbsp;&nbsp;background: -o-linear-gradient(${bgColor1}, ${bgColor2.value});<br>` +
      `&nbsp;&nbsp;background: -moz-linear-gradient(${bgColor1}, ${bgColor2.value});<br>` +
      `&nbsp;&nbsp;background: linear-gradient(${bgColor1}, ${bgColor2.value});`;

    fillCode();
  } else {
    //disable gradient colors
    bgColor2.disabled = true;
    bgColor2Label.classList.add('disabled');

    customButton.style.background = bgColor1;
    customCode[2] = `background-color: ${bgColor1};`;

    fillCode();
  }

  if (gradientCheckH){
    //enable gradient colors
    bgColor2H.disabled = false;
    bgColor2LabelH.classList.remove('disabled');

    customButtonH.style.background = getCssValuePrefix() + 'linear-gradient( ' +
      bgColor1H + ', ' + bgColor2H.value + ')';

    customCodeH[2] =
      `background: -webkit-linear-gradient(${bgColor1H}, ${bgColor2H.value});<br>` +
      `&nbsp;&nbsp;background: -o-linear-gradient(${bgColor1H}, ${bgColor2H.value});<br>` +
      `&nbsp;&nbsp;background: -moz-linear-gradient(${bgColor1H}, ${bgColor2H.value});<br>` +
      `&nbsp;&nbsp;background: linear-gradient(${bgColor1}, ${bgColor2H.value});`;

    fillCodeH();
  } else {
    //disable gradient colors
    bgColor2H.disabled = true;
    bgColor2LabelH.classList.add('disabled');

    customButtonH.style.background = bgColor1H;
    customCodeH[2] = `background-color: ${bgColor1H};`;

    fillCodeH();
  }
}

function roundEdges() {
  let roundEdges = document.getElementById('roundEdges').value;

  customButton.style.borderRadius = roundEdges + '%';
  customButtonH.style.borderRadius = roundEdges + '%';

  if (roundEdges === '0'){
    //disable round edges
    customCode[3] = 'false';
  } else {
    //enable round edges
    customCode[3] = `border-radius: ${roundEdges}%;`;
  }
  fillCode();
}

function border() {
  let borderStyle = document.getElementById('borderStyle').value;
  let borderWidth = document.getElementById('borderWidth');
  let borderColor = document.getElementById('borderColor');
  let borderWidthLabel = document.getElementById('borderWidthLabel');
  let borderColorLabel = document.getElementById('borderColorLabel');

  if (borderStyle === 'None') {
    //disable border styling
    borderColor.disabled = true;
    borderWidth.disabled = true;
    borderWidthLabel.classList.add('disabled');
    borderColorLabel.classList.add('disabled');
    customButton.style.border = 'none';
    customButtonH.style.border = 'none';
    customCode[4] = 'false';
  } else {
    //enable border styling
    borderColor.disabled = false;
    borderWidth.disabled = false;
    borderWidthLabel.classList.remove('disabled');
    borderColorLabel.classList.remove('disabled');
    customButton.style.border = borderWidth.value + 'px ' + borderStyle + ' ' + borderColor.value;
    customButtonH.style.border = borderWidth.value + 'px ' + borderStyle + ' ' + borderColor.value;
    customCode[4] = `border: ${borderWidth.value}px ${borderStyle} ${borderColor.value};`;
  }
  fillCode();
}

function buttonPadding() {
  let buttonPadding = document.getElementById('buttonPadding').value;

  customButton.style.padding = buttonPadding + 'px';
  customButtonH.style.padding = buttonPadding + 'px';

  if (buttonPadding === '0') {
    customCode[5] = 'false';
  } else {
    customCode[5] = `padding: ${buttonPadding}px;`;
  }
  fillCode();
}

function textShadow() {
  let textShadowDistance = document.getElementById('textShadowDistance').value;
  let textShadowBlur = document.getElementById('textShadowBlur').value;
  let textShadowColor = document.getElementById('textShadowColor').value;

  let textShadowDistanceH = document.getElementById('textShadowDistanceH').value;
  let textShadowBlurH = document.getElementById('textShadowBlurH').value;
  let textShadowColorH = document.getElementById('textShadowColorH').value;

  customButton.style.textShadow =
    textShadowDistance + 'px ' + textShadowDistance + 'px ' +
    textShadowBlur + 'px ' + textShadowColor;
  customCode[6] =
    `text-shadow: ${textShadowDistance}px ${textShadowDistance}px ${textShadowBlur}px ${textShadowColor};`;

  if (textShadowDistance == 0 && textShadowBlur == 0) {
    customCode[6] =  'false';
  }

  customButtonH.style.textShadow =
    textShadowDistanceH + 'px ' + textShadowDistanceH + 'px ' +
    textShadowBlurH + 'px ' + textShadowColorH;
  customCodeH[3] =
    `text-shadow: ${textShadowDistanceH}px ${textShadowDistanceH}px ${textShadowBlurH}px ${textShadowColorH};`;

  if (textShadowDistanceH == 0 && textShadowBlurH == 0) {
    customCodeH[3] =  'false';
  }

  fillCode();
  fillCodeH();
}

function boxShadow() {
  let boxShadowDistance = document.getElementById('boxShadowDistance').value;
  let boxShadowBlur = document.getElementById('boxShadowBlur').value;
  let boxShadowColor = document.getElementById('boxShadowColor').value;

  let boxShadowDistanceH = document.getElementById('boxShadowDistanceH').value;
  let boxShadowBlurH = document.getElementById('boxShadowBlurH').value;
  let boxShadowColorH = document.getElementById('boxShadowColorH').value;

  customButton.style.boxShadow =
    boxShadowDistance + 'px ' + boxShadowDistance + 'px ' +
    boxShadowBlur + 'px ' + boxShadowColor;
  customCode[7] =
    `box-shadow: ${boxShadowDistance}px ${boxShadowDistance}px ${boxShadowBlur}px ${boxShadowColor};`;

  if (boxShadowDistance == 0 && boxShadowBlur == 0) {
    customCode[7] =  'false';
  }

  customButtonH.style.boxShadow =
    boxShadowDistanceH + 'px ' + boxShadowDistanceH + 'px ' +
    boxShadowBlurH + 'px ' + boxShadowColorH;
  customCodeH[4] =
    `box-shadow: ${boxShadowDistanceH}px ${boxShadowDistanceH}px ${boxShadowBlurH}px ${boxShadowColorH};`;

  if (boxShadowDistanceH == 0 && boxShadowBlurH == 0) {
    customCodeH[4] =  'false';
  }

  fillCode();
  fillCodeH();
}

//enable/disable the hover settings
function hoverSwitch() {
  let hoverCheck = document.getElementById('hoverCheck').checked;
  let hoverToggle = document.getElementById('hoverToggle');
  let hoverToggleInput = hoverToggle.getElementsByTagName('input');

  if (hoverCheck) {
    //enable hover options
    customButtonH.style.visibility = 'visible';
    hoverToggle.classList.remove('disabled');

    for (let i = 0; i < hoverToggleInput.length; i++) {
      hoverToggleInput[i].disabled = false;
    }

    return fillCodeH(true);
  } else {
    //disable hover options
    customButtonH.style.visibility = 'hidden';
    hoverToggle.classList.add('disabled');

    for (let i = 0; i < hoverToggleInput.length; i++) {
      hoverToggleInput[i].disabled = true;
    }

    return fillCodeH(false);
  }
}

//fills the codebox with css code for the user to copy
function fillCodeH(isActive) {
  let hoverCode = document.getElementById('hoverCode');
  let hoverCodeArea = document.getElementById('hoverCodeArea');
  hoverCodeArea.innerHTML = '';

  if (isActive === false) {
    //hides .custom-button:hover when checked
    hoverCode.style.display = 'none';
  }
  else {
    hoverCode.style.display = 'block';
    customCodeH.forEach(function(item){
      //displays everything in the customCodeH array except for ones with 'false'
      if (item !== 'false') {
        hoverCodeArea.innerHTML += ("&nbsp;&nbsp;" + item + "<br>");
      }
    });
  }
}

//when user mouses over the test button, .custom-button:hover changes will be shown
function toggleHover() {
  if (customButtonH.style.display === 'none'){
  customButtonH.style.display = 'block';
  } else {
    customButtonH.style.display = 'none';
  }
}

//tab toggle function
function settingsTab(evt, tabName) {
  let i, settingsContainer, settingsLink;

    settingsContainer = document.getElementsByClassName("settingsContainer");
    for (i = 0; i < settingsContainer.length; i++) {
        settingsContainer[i].style.display = "none";
    }

    settingsLink = document.getElementsByClassName("settingsLink");
    for (i = 0; i < settingsLink.length; i++) {
        settingsLink[i].className = settingsLink[i].className.replace(" active", "");
    }

    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

//to discover the user's browser so a linear background can properly display
function getCssValuePrefix() {
  const prefixes = ['-o-', '-ms-', '-moz-', '-webkit-'];
  let browserPrefix;
  let testDiv = document.createElement('div');

  for (let i = 0; i < prefixes.length; i++) {
    testDiv.style.background = prefixes[i] + 'linear-gradient(#000000, #ffffff)';

    if (testDiv.style.background) {
      browserPrefix = prefixes[i];
    }
  }
  testDiv = null;

  delete testDiv;
  return browserPrefix;
}
